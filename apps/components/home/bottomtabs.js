import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import GreenScreen from './greenscreen';
import RedScreen from './redscreen';
import AsyncStorage from '@react-native-community/async-storage';

const Tab = createBottomTabNavigator();

class Bottomtabs extends React.Component {
    render() {
        let a = AsyncStorage.getItem('name');
        console.log(a);
        return(
        <Tab.Navigator>
                <Tab.Screen name="GreenScreen" component={GreenScreen}/>
                <Tab.Screen name="RedScreen" component={RedScreen} />
        </Tab.Navigator>
        );
    }
}

export default Bottomtabs;