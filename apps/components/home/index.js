import React from 'react';
import {Text, View, Button} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import BlueScreen from './bluescreen';
import DefaultScreen from './defaultscreen';
import BottomTabs from './bottomtabs';
import AsyncStorage from '@react-native-community/async-storage';
import Logi from '../../../App';

const Drawer=createDrawerNavigator();
const Tab = createBottomTabNavigator();

class Home extends React.Component {

    signOut() {
        AsyncStorage.removeItem('userToken');
        alert("Logged Out");
    }
    
    render() {
        return (
            <>
            <Drawer.Navigator >
                <Drawer.Screen name="BottomTabs" component={BottomTabs}/>
                <Drawer.Screen name="DefaultScreen" component={DefaultScreen}/>
                <Drawer.Screen name="BlueScreen" component={BlueScreen}/>
            </Drawer.Navigator>
            <Button title="Sign Out" onPress={() => this.signOut()} />
            </>
        );
    }

}

export default Home;