import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import GreenScreen from './greenscreen';
import RedScreen from './redscreen';
import DefaultScreen from './defaultscreen';

const Tab = createBottomTabNavigator();

export default class BlueScreen extends Component {
    render() {
    return (
        <>
        <Text style={styles.title}>Blue Screen</Text>
        
        </>
    );
    }
 }
  const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    }
 });
