import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

export default class RedScreen extends Component {
    render() {
        AsyncStorage.removeItem('name')
    return (
        <View style={styles.container}>
        <Text style={styles.title}>Red Screen</Text>
        </View>
    );
    }
 }
  const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    }
 });
