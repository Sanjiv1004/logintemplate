import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import GreenScreen from './greenscreen';
import RedScreen from './redscreen';

const Tab= createBottomTabNavigator();

export default class DefaultScreen extends Component {
 render() {
   return (
       <>
       <Text style={styles.title}>Default Screen</Text>
       </>
   );
 }
}
const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    }
 });
 