import React from 'react';
import {TextInput,Button,Text, View} from 'react-native';
import OtpInputs from "react-native-otp-inputs";
import AsyncStorage from '@react-native-community/async-storage';

class otp extends React.Component {
    render() {
        return (
            <View>
                <Text style={{fontSize:20,alignSelf:'center'}}>Enter OTP</Text>
                <View style={{alignSelf:'center'}}>
                <OtpInputs
                handleChange={code => AsyncStorage.setItem('otp',code)}
                numberOfInputs={6}
                />
                </View>
            </View>
        )
    }
}

export default otp;