import React from 'react';
import { View, Button, TextInput,Image } from 'react-native';
import FilePickerManager from 'react-native-file-picker';
import otp from '../otp'

class SignUp extends React.Component {
  constructor(props) {
    super(props)
    this.state={
      file:'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png'
    }
  }

  selectOneFile() {
    FilePickerManager.showFilePicker(null, (response) => {
      console.log('Response = ', response);
     
      if (response.didCancel) {
        console.log('User cancelled file picker');
      }
      else if (response.error) {
        console.log('FilePickerManager Error: ', response.error);
      }
      else {
        this.setState({
          file: response.uri
        });
        console.log("state =",this.state.file)
      }
    });
  }

  otp() {
    this.props.navigation.navigate('otp');
  }
    render() {
      let a=this.state.file;
        return (
            <View>
                <Image  style = {{alignSelf:'center', width: 100, height: 100 }} source={{uri:a}} />
                <TextInput placeholder="First Name"></TextInput>
                <TextInput placeholder="Middle Name"></TextInput>
                <TextInput placeholder="Last Name"></TextInput>
                <TextInput placeholder="E-mail"></TextInput>
                <TextInput placeholder="Phone"></TextInput>
                <TextInput placeholder="Password"></TextInput>
                <TextInput placeholder="Confirm Password"></TextInput>
                <Button title="upload image" onPress={() => this.selectOneFile()} />
                <Button title="Sign Up" onPress={() => this.otp()} />       
            </View>
        );
    }
}

export default SignUp;