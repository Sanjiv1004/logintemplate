import React from 'react';
import { View,StyleSheet, Button, Alert } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import  Home  from '../home';
import SignUp from '../registration';
import AsyncStorage from '@react-native-community/async-storage';

class Login extends React.Component {


    constructor() {
        super()
        this.state={username:"",password:""}
    }

    checkLogin() {
        let name = 'Michal';  
        AsyncStorage.setItem('name',name); 
        this.props.navigation.navigate('Home');    
    }

    reg() {
        this.props.navigation.navigate('Sign Up')
    }

    render() {
        return (
            <View >
                <TextInput placeholder="Username" onChangeText={text => this.setState({username:text})}></TextInput>
                <TextInput placeholder="Password" onChangeText={text => this.setState({password:text})}></TextInput>
                <Button title="Log In" onPress={() => this.checkLogin()} />
                <Button title="Sign Up" onPress={() => this.reg()} />
            </View>
        );
    }
}

export default Login;



          
          
